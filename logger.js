var winston = require('winston');
var moment = require('moment');

var logger = new winston.Logger({
	transports: [
		new winston.transports.Console({
			level: 'debug',
			handleExceptions: true,
			json: false,
			colorize: true,
            timestamp: function () {
                return moment().format('DD/MM/YYYY hh:mm:ss A');
            }
		})
	],
	exitOnError: false
});

module.exports = logger;