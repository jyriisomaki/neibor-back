var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var base64Img = require('base64-img');
var async = require('async');
var jimp = require('jimp');
var Car = require('./car');

var userSchema = mongoose.Schema({
  firstname: String,
  lastname: String,
  email: String,
  phone: String,
  password: String,
  address: String,
  city: String,
  postcode: Number,
  coords: { lat: Number, lng: Number },
  cars: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Car'
    }
  ],
  reviews: [
    {
      user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
      },
      desc: String,
      stars: Number,
      ctime: {
        type: Date,
        default: Date.now, index: true
      }
    }
  ],
  img: String
});

userSchema.set('usePushEach', true);

userSchema.pre('save', function(next) {  
  if(this.isNew || this.isModified('password')) {
    var profile = this;
  
    bcrypt.hash(profile.password, 10).then(function(hash) {
      profile.password = hash;
      next();
    }).catch(next);
  } else {
    next();
  }
});

var User = mongoose.model('User', userSchema);

module.exports = User;

module.exports.comparePassword = function(candidatePassword, hash) {
  return bcrypt.compare(candidatePassword, hash).then(function(isMatch) {
    return isMatch;
  });
}

module.exports.editProfile = function(oldProfile, editedProfile) {
  return User.findOne({ _id: oldProfile._id }).then(function(profile) {
    if(!profile) {
      return false;
    } else {
      profile.firstname = editedProfile.firstname;
      profile.lastname = editedProfile.lastname;
      profile.phone = editedProfile.phone;
      profile.address = editedProfile.address;
      profile.city = editedProfile.city;
      profile.postcode = editedProfile.postcode;
      profile.coords = editedProfile.coords;

      if(editedProfile.newPassword && editedProfile.newPasswordAgain) {
        profile.password = editedProfile.newPassword;
      }
    };
  });
}