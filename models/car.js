var mongoose = require('mongoose');
var base64Img = require('base64-img');
var jimp = require('jimp');
var User = require('./user');

var carSchema = mongoose.Schema({
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User'
	},
	brand: String,
	model: String,
	plate: String,
	gear: String,
	fuel: String,
	year: Number,
	hPrice: Number,
	dPrice: Number,
	kmPrice: Number,
	desc: String,
	img: []
});

carSchema.set('usePushEach', true);

var Car = mongoose.model('Car', carSchema);

module.exports = Car;

module.exports.editCar = function(car, editedCar, cb) {
	car.hPrice = editedCar.hPrice;
	car.dPrice = editedCar.dPrice;
	car.kmPrice = editedCar.kmPrice;
	car.desc = editedCar.desc;

	return car.save();
}

module.exports.convertImage = function(id, image, index) {
	return new Promise(function(resolve, reject) {
		base64Img.img(image, './img/cars/' + id, index, function(err, filepath) {
			if(err) {
				return reject(err);
			} else {
				return resolve(filepath);
			}
		});
	}).then(function(filepath) {
		return jimp.read(filepath).then(function(image) {
			image.resize(jimp.AUTO, 500).write(filepath)

			return filepath;
		})
	});
}