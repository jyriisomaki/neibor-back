var mongoose = require('mongoose');

var brandSchema = mongoose.Schema({
	name: String,
	models: []
});

brandSchema.set('usePushEach', true);

var Brand = mongoose.model('Brand', brandSchema);

module.exports = Brand;