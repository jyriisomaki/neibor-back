var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var User = require('../models/user');
var config = require('../config/database');

module.exports = function(passport) {
	var opts = {};

	opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt');
	opts.secretOrKey = config.secret;

	passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
		User.findOne({ '_id': jwt_payload._id }).
		populate('cars').
		populate('reviews.user', '_id firstname').
		exec(function(err, user) {
			if(err) {
				return done(err, false);
			}

			if(user) {
				return done(null, user);
			} else {
				done(null, false);
			}
		});
	}))
}