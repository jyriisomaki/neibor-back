module.exports = {
	secret: 'neiborsecret',
	database: 'mongodb://localhost:27017/neibor',
	options: {
		useMongoClient: true
	}
}