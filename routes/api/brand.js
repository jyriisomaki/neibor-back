var express = require('express');
var mongoose = require('mongoose');
var Brand = mongoose.model('Brand');
var router = express.Router();

router.get('/', function(req, res, next) {
	Brand.find().then(function(brands) {
		if(!brands) {
			return res.status(404).send('Brands not found');
		} else {
			return res.json(brands);
		}
	}).catch(function(err) {
		return next(err);
	});
});

module.exports = router;