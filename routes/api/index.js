var express = require('express');
var router = express.Router();

router.use('/profile', require('./profile'));
router.use('/user', require('./user'));
router.use('/brand', require('./brand'));
router.use('/car', require('./car'));

module.exports = router;