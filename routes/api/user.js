var express = require('express');
var mongoose = require('mongoose');
var passport = require('passport');
var jwt = require('jsonwebtoken');
var config = require('../../config/database');
var logger = require('../../logger');
var User = mongoose.model('User');
var router = express.Router();

router.get('/', function(req, res, next) {
	User.find().
	select('-password').
	select('-reviews').
	then(function(users) {
		if(!users.length) {
			return res.status(404).send('Users not found');
		} else {
			return res.json(users);
		}
	}).catch(function(err) {
		return next(err);
	});
});

router.get('/:id', function(req, res, next) {
	User.findOne({ _id: req.params.id }).
	populate('cars').
	populate('reviews.user', '_id firstname').
	select('-password').
	then(function(user) {
		if(!user) {
			return res.status(404).send('User not found');
		} else {
			return res.json(user);
		}
	}).catch(function(err) {
		return next(err);
	});;
});

router.put('/:id', passport.authenticate('jwt', { session: false }), function(req, res, next) {
	User.findOne({ _id: req.params.id }).then(function(user) {
		return user;
	}).then(function(user) {
		user.reviews.push({
			user: req.user._id,
			desc: req.body.desc,
			stars: req.body.stars
		});

		return user;
	}).then(function(user) {
		res.sendStatus(200);
		logger.info('Comment added for user ' + user._id);		
	}).catch(function(err) {
		return next(err);	
	});
});

router.post('/register', function(req, res, next) {
	var newUser = new User({
		firstname: req.body.firstname,
		lastname: req.body.lastname,
		email: req.body.email,
		phone: req.body.phone,
		password: req.body.password,
		address: req.body.address,
		city: req.body.city,
		postcode: req.body.postcode,
		coords: req.body.coords
	});

	User.findOne({ email: newUser.email }).then(function(isReserved) {
		if(isReserved) {
			return res.status(403).send('User already exists');
		} else {
			res.sendStatus(200);

			logger.info('User ' + newUser._id + ' registered');

			return newUser.save();
		}
	}).catch(function(err) {
		return next(err);
	});
});

router.post('/login', function(req, res, next) {
	var email = req.body.email;
	var password = req.body.password;

	User.findOne({ email: email }).then(function(user) {
		if(!user) {
			res.sendStatus(404).send('User not found');
			return;
		} else {
			return user;
		}
	}).then(function(user) {
		User.comparePassword(password, user.password).then(function(isMatch) {
			if(isMatch) {
				var token = jwt.sign(user.toObject(), config.secret, {
					expiresIn: 604800 // one week
				});

				res.json({
					success: true,
					token: 'JWT ' + token
				});
			} else {
				res.status(403).send('Wrong password');
			}
		});			
	}).catch(function(err) {
		return next(err);
	});
});

module.exports = router;