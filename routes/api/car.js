var express = require('express');
var mongoose = require('mongoose');
var jimp = require('jimp');
var async = require('async');
var passport = require('passport');
var logger = require('../../logger');
var Car = mongoose.model('Car');
var User = mongoose.model('User');
var router = express.Router();

router.get('/', function(req, res, next) {
	Car.find().then(function(cars) {
		if(!cars) {
			return res.status(404).send('Cars not found');
		} else {
			return res.json(cars);
		}
	}).catch(function(err) {
		return next(err);
	});
});

router.get('/:id', function(req, res, next) {
	Car.findOne({ _id: req.params.id }).populate('user', '-password').then(function(car) {
		if(car) {
			return res.json(car);
		} else {
			return res.status(404).send('Car not found');
		}
	}).catch(function(err) {
		return next(err);
	});
});

router.put('/:id/edit', function(req, res, next) {
	Car.findOne({ _id: req.params.id }).then(function(car) {
		return car;
	}).then(function(car) {
		return Car.editCar(car, req.body);
	}).then(function(car) {
		logger.info('Car ' + car._id + ' edited');

		return res.status(200).send('Car edited');
	}).catch(function(err) {
		return next(err);	
	});
});

router.post('/', passport.authenticate('jwt', { session: false }), function(req, res, next) {
	var user = {};
	var car = new Car({
		user: req.user._id,
		brand: req.body.brand,
		model: req.body.model,
		plate: req.body.plate,
		gear: req.body.gear,
		fuel: req.body.fuel,
		year: req.body.year,
		hPrice: req.body.hPrice,
		dPrice: req.body.dPrice,
		kmPrice: req.body.kmPrice,
		desc: req.body.desc,
		img: []
	});

	User.findOne({ _id: req.user._id }).then(function(foundUser) {
		user = foundUser;

		return Promise.all(req.body.imgData.map(function(image, index) {
			return Car.convertImage(car._id, image, index);
		}));
	}).then(function(filepaths) {
		car.img = filepaths.map(function(path) {
			return path.replace('img/cars', '');
		});

		user.cars.push(car._id);

		car.save();
		user.save();
	}).then(function() {
		logger.info('Car ' + car._id + ' saved to database');
		logger.info('Car ' + car._id + ' saved to user ' + user._id);
		
		return res.status(200).send({car_id: car.id});
	}).catch(function(err) {
		return next(err);
	})
});

module.exports = router;