var express = require('express');
var mongoose = require('mongoose');
var passport = require('passport');
var logger = require('../../logger');
var Profile = mongoose.model('User');
var router = express.Router();

router.get('/', passport.authenticate('jwt', { session: false }), function(req, res) {
	res.json(req.user);
});

router.put('/', passport.authenticate('jwt', { session: false }), function(req, res, next) {
	var oldProfile = req.user;
	var editedProfile = req.body;

	Profile.editProfile(oldProfile, editedProfile).then(function(profile) {
		if(profile) {
			res.status(200).send('Profile edited')
			logger.info('Profile ' + profile._id + ' edited');
		} else {
			res.sendStatus(500);
		}
	}).catch(function(err) {
		return next(err);
	});
});

module.exports = router;