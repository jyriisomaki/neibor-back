var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var passport = require('passport');
var fs = require('file-system');
var cors = require('cors');
var config = require(__dirname + '/config/database.js');

var app = express();
var port = 1337;

// MongoDB
mongoose.Promise = global.Promise;

mongoose.connect(config.database, config.options, function(err) {
	if(err) {
		console.log('Database error: ' + err);
  	}
});

// CORS
app.use(cors());

// Passport
app.use(passport.initialize());
app.use(passport.session());
require(__dirname + '/config/passport')(passport);

// Body Parser
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

// Pretty JSON
app.set('json spaces', 4);

// Logger
var logger = require('./logger');

// Models
var User = require(__dirname+ '/models/user');
var Car = require(__dirname + '/models/car');
var Brand = require(__dirname + '/models/brand');

// Routes
app.use(require(__dirname + '/routes'));

// Error handling
app.use(function(err, req, res, next) {
	logger.info(err.message);

    res.status(err.statusCode || 500).send(err.message);
});

app.listen(port, function() {
	fs.readFile(__dirname + '/neibor.txt', 'utf8', function(err, data) {
		console.log(data);
		console.log('Port: ' + port);
		console.log('Database: ' + config.database);
	})
});
